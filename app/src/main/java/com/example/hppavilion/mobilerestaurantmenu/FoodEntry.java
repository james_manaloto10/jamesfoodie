package com.example.hppavilion.mobilerestaurantmenu;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.List;

public class FoodEntry extends AppCompatActivity {

    Button submit;
    EditText fname, fdesc, fpr, fqty;
    Intent feIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_food_entry);

        submit = (Button) (findViewById(R.id.fb));
        fname = (EditText) (findViewById(R.id.fn));
        fdesc = (EditText) (findViewById(R.id.fd));
        fpr = (EditText) (findViewById(R.id.fp));
        fqty = (EditText) (findViewById(R.id.fq));
        feIntent = new Intent(FoodEntry.this, Lis.class);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if ((fname != null) && (fdesc != null) && (fpr != null) && (fqty != null)) {
                    Toast.makeText(getApplicationContext(), "Added Data Successfully.", Toast.LENGTH_LONG).show();
                    feIntent.putExtra("name", fname.getText().toString());
                    feIntent.putExtra("desc", fdesc.getText().toString());
                    feIntent.putExtra("prce", fpr.getText());
                    feIntent.putExtra("qnty", fqty.getText());
                   // Intent feIntent = new Intent(FoodEntry.this, List.class);
                    startActivity(feIntent);
                    finish();
                } else {
                    Toast.makeText(getApplicationContext(), "Please enter name, description, price and quantity.", Toast.LENGTH_LONG).show();
                }
            }

        });

//Gela

    }

}
